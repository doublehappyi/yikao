var db = require('../conf').db;
var table = db.table,
    pool = db.pool;
   
var sql = {
    queryByDistrictAndMajorcate:'select id, name, logo, tag from school where id in (select school_id from school_majorcate_district where district_id=? and majorcate_id=?) limit ?, ?',
    queryByDistrict:'select id, name, logo, tag from school where id in (select school_id from school_majorcate_district where district_id=?) limit ?, ?',
    queryByMajorcate:'select id, name, logo, tag from school where id in (select school_id from school_majorcate_district where majorcate_id=?) limit ?, ?',
    queryAll:'select id, name, logo, tag from school limit ?, ?',
    queryById:'select id, name, logo, banner, tag, stype, nature, recruitOrder, recruitTotalNum, address, telephone from ' + db.table.school + ' where id=?',
    queryEnrollmentById:'select enrollment from '+db.table.school + ' where id=?',
    queryIntroById:'select intro from '+db.table.school + ' where id=?'
}


module.exports = {
    queryByDistrictAndMajorcate:function (district_id, majorcate_id, page, callback){
        pool.getConnection(function(err, conn){
            if (err) { callback(err);return;}
            
            var pageSize = 40;
            var start = pageSize*(page - 1) + 1;
            var sql_str, sql_params;
            
            if(district_id === 0 && majorcate_id === 0){
                sql_str = sql.queryAll, sql_params = [start, pageSize];
            }else if(district_id === 0 && majorcate_id !== 0){
                sql_str = sql.queryByMajorcate, sql_params = [majorcate_id, start, pageSize];
            }else if(district_id !== 0 && majorcate_id === 0){
                sql_str = sql.queryByDistrict, sql_params = [district_id, start, pageSize];
            }else if(district_id !== 0 && majorcate_id !== 0){
                sql_str = sql.queryByDistrictAndMajorcate, sql_params = [district_id, majorcate_id, start, pageSize]
            }
            
            conn.query(sql_str, sql_params, function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        }); 
    },
    queryById:function(id, callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryById, [id], function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    },
    queryIntroById:function(school_id, callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryIntroById,[school_id], function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    },
    queryEnrollmentById:function(school_id, callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryEnrollmentById,[school_id], function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    }
}