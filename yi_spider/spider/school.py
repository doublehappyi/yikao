#coding:utf-8
__author__ = 'yishuangxi'
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from bs4 import BeautifulSoup
import db
import utils
import json


def get_dis_rows():
    conn = db.getConnection()
    cursor = db.getCursor(conn)
    select_str = 'select * from {0}'.format(db.settings['table']['district'])
    cursor.execute(select_str)
    conn.close()
    cursor.close()

    return cursor


def get_school_items(dis_rows):
    school_items = []
    for row in dis_rows:
        dis_id, dis_num = row[0], row[1]
        page = 1
        while 1:
            url = 'http://yikaoapp.com/share/ajax.php?page=' + str(page) + '&from=&dis=' + dis_num + '&spc=&key='
            html = utils.get_html(url)
            # print html
            data_items = json.loads(html)
            data_items = data_items[0:-1]
            for data_item in data_items:
                data_item['district_id'] = dis_id

            school_items += data_items
            if len(data_items) < 41: break
            page += 1

    return school_items


def spider(school_items):
    conn = db.getConnection()
    cursor = db.getCursor(conn)
    for school_item in school_items:
        url = 'http://yikaoapp.com/share/?school_id=' + str(school_item['id']) + '&from='
        html = utils.get_html(url)

        school_analyze(html, school_item, conn, cursor)
        major_analyze(html, school_item, conn, cursor)

        # conn.close()
        # cursor.close()

def major_analyze(html, school_item, conn, cursor):
    s_id = school_item['id']
    base_url = 'http://yikaoapp.com/share/'
    bs = BeautifulSoup(html, 'html.parser')

    topItem = bs.select('.s_top2')[0]

    cItems = topItem.select('div.clearfix')
    for cItem in cItems:
        academyName = cItem.select('.s_title > span')[0].get_text()
        majorItems = cItem.select('.s_item')
        for mItem in majorItems:
            m_name = mItem.select('a')[0].get_text().split('招生人数')[0].strip()

            select_str = "select id from {0}".format(db.settings['table']['major']) + " where school_id=%s and name=%s"
            if cursor.execute(select_str, (s_id, m_name)):
                print 'major ' + m_name + ' exists !!!!!!!!!'
                return

            m_recruit = mItem.select('a > span')[0].get_text().split('：')[1]
            m_detail = utils.get_re_html(base_url + mItem.select('a')[0].attrs['href'])
            majorCate_id = get_majorCate_id(m_name)

            # print m_name, m_recruit, str(majorCate_id), str(s_id)
            try:
                print 'major '  + m_name +' inserting ....'
                insert_str = "insert into {0}".format(db.settings['table']['major']) +  " values (null, %s, %s, %s, %s, %s, %s)"
                cursor.execute(insert_str, (m_name, m_recruit, m_detail, academyName, majorCate_id, s_id))
                conn.commit()
            except Exception as e:
                print 'major Exception {0}'.format(e)


def school_analyze(html, school_item, conn, cursor):
    base_url = 'http://yikaoapp.com/share/'
    bs = BeautifulSoup(html, 'html.parser')
    s_id = school_item['id']

    select_str = "select id from {0}".format(db.settings['table']['school']) + " where id=%s"
    if cursor.execute(select_str, (s_id, )):
        print 'school '+str(s_id)+' exists !!!!!!!!!'
        return


    s_name = school_item['name']
    s_logo = 'http://i.yikaoapp.com/upload/school/'+s_id+'/' + school_item['logo']
    s_banner =bs.select('#topimg')[0].attrs['src']
    s_tag = school_item['tag']

    # print 's_id, s_name, s_logo, s_banner, s_tag:', s_id, s_name, s_logo, s_banner, s_tag

    infoItem = bs.select('.s_info > div')
    s_type = infoItem[0].get_text().split('：')[1]
    s_nature = infoItem[1].get_text().split('：')[1]
    s_recruitOrder = infoItem[2].get_text().split('：')[1]
    s_recruitTotalNum = infoItem[3].get_text().split('：')[1]

    # print 's_type, s_nature, s_recruitOrder, s_recruitTotalNum ',s_type, s_nature, s_recruitOrder, s_recruitTotalNum

    enrollItem = bs.select('.s_jianzhang a')
    if len(enrollItem) > 0:
        enrollment_url = base_url + enrollItem[0].attrs['href']
        s_enrollment = utils.get_re_html(enrollment_url)
    else:
        s_enrollment = None

    introItem = bs.select('.s_top2')[1].select('.s_item > a')
    if (len(introItem)) > 0:
        intro_url = base_url + introItem[0].attrs['href']
        s_intro = utils.get_re_html(intro_url)
        s_address = introItem[1].get_text().split('：')[1]
        s_telephone = introItem[2].get_text().split('：')[1]
    else:
        s_intro, address, telephone = None, None, None

    district_id = school_item['district_id']

    # print s_enrollment
    # print s_intro

    # print 's_address, s_telephone, district_id', s_address, s_telephone, district_id

    print 'school '+str(s_id)+' inserting ...'
    try:
        insert_str = "insert into {0}".format(db.settings['table']['school']) +  " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(insert_str, (s_id, s_name, s_logo, s_banner, s_tag, s_type, s_nature, s_recruitTotalNum, s_recruitOrder, s_enrollment, s_intro, s_address, s_telephone, district_id))
        conn.commit()
    except Exception as e:
        print 'school Exception {0}'.format(e)

def get_majorCate_id(name):
    #"音乐类":1, "播音类":2,"编导类":3,"舞蹈类":4 ,"美术类":5, "表演类":6, "摄影类":7, "模特类":8, "空乘类":9, "其它类"
    pro_type = 10
    if (u"音乐" or  u"声乐" or  u"美声") in name:
        pro_type = 1
    elif (u"播音" or u"主持") in name:
        pro_type = 2
    elif (u"编导" or u"导演" or u"编辑") in name:
        pro_type = 3
    elif u"舞蹈" in name:
        pro_type = 4
    elif (u"美术" or u"画"  or u"舞台") in name:
        pro_type = 5
    elif (u"表演" or u"话剧") in name:
        pro_type = 6
    elif (u"摄影" or u"光学" or u"照明") in name:
        pro_type = 7
    elif u"模特" in name:
        pro_type = 8
    elif u"空乘" in name:
        pro_type = 9
    else:
        pro_type = 10

    return pro_type

if __name__ == '__main__':
    dis_rows = get_dis_rows()
    school_items = get_school_items(dis_rows)
    print len(school_items)
    # school_items = school_items[0:2]
    spider(school_items)
