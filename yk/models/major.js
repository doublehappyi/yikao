var db = require('../conf').db;
var table = db.table,
    pool = db.pool;
    
var sql = {
    queryBySchoolId:'select id, name, recruit, academyName from ' + db.table.major + ' where school_id=?',
    queryAll:'select id, name from ' + db.table.majorcate + ' order by id',
    queryDetailById:'select detail from ' + db.table.major + ' where id=?'
}

module.exports = {
    queryBySchoolId:function(school_id, callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryBySchoolId, [school_id], function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    },
    queryAll:function(callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryAll, function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    },
    queryDetailById:function(major_id, callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryDetailById,[major_id], function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    }
}