###数据库
CREATE DATABASE IF NOT EXISTS yk;
use yk;
###地区表
CREATE TABLE IF NOT EXISTS district(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  num VARCHAR(10) NOT NULL,
  name VARCHAR(50) UNIQUE NOT NULL
);

###专业类目表：专业类目不从属于任何学校或者地区
CREATE TABLE IF NOT EXISTS majorcate(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(50) UNIQUE NOT NULL
);

#学校／专业分类／地区关系表
create table school_majorcate_district(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  school_id INT(10) NOT NULL,
  majorCate_id INT(10) NOT NULL,
  district_id INT(10) NOT NULL
);

###学校表
CREATE TABLE IF NOT EXISTS school(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(50) UNIQUE NOT NULL,
  logo VARCHAR(200) DEFAULT NULL,#学校校徽
  banner VARCHAR(200) DEFAULT NULL,#学校详情页top展示图
  tag VARCHAR(50) NOT NULL,###学校标签
  stype VARCHAR(50) NOT NULL,###学校类型
  nature VARCHAR(50) NOT NULL,###学校性质
  recruitOrder VARCHAR(50) NOT NULL,###录取批次
  recruitTotalNum VARCHAR(50) NOT NULL, #招生总人数
  enrollment MEDIUMTEXT NOT NULL,###招生简章
  intro MEDIUMTEXT NOT NULL,###学校简介
  address VARCHAR(100) NOT NULL,###地址
  telephone VARCHAR(50) NOT NULL,###电话
  district_id VARCHAR(50) NOT NULL ###学校所属地区代号
);

###专业表
CREATE TABLE IF NOT EXISTS major(
  id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  name VARCHAR(100) NOT NULL,
  ###招生信息
  recruit VARCHAR(100) DEFAULT NULL,
  ###专业详情页
  detail MEDIUMTEXT DEFAULT NULL,
  academyName VARCHAR(100) DEFAULT NULL,###学院名字
  majorCate_id INT(10) NOT NULL,###专业从属于某一个类目
  school_id INT(10) NOT NULL ###专业从属于某一个学校
);
#"音乐类":1, "播音类":2,"编导类":3,"舞蹈类":4 ,"美术类":5, "表演类":6, "摄影类":7, "模特类":8, "空乘类":9, "其它类":10

INSERT INTO majorCate VALUES(1, "音乐类");
INSERT INTO majorCate VALUES(2, "播音类");
INSERT INTO majorCate VALUES(3, "编导类");
INSERT INTO majorCate VALUES(4, "舞蹈类");
INSERT INTO majorCate VALUES(5 ,"美术类");
INSERT INTO majorCate VALUES(6, "表演类");
INSERT INTO majorCate VALUES(7, "摄影类");
INSERT INTO majorCate VALUES(8, "模特类");
INSERT INTO majorCate VALUES(9, "空乘类");
INSERT INTO majorCate VALUES(10,"其它类");