#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector,Selector
from yikao.items import YikaoItem
from scrapy.http import FormRequest,Request
from scrapy import log
import json
import requests

class Mi2Spider(CrawlSpider):
    name = 'yishu'
    allowed_domains = ["yikaoapp.com"]
    start_urls = [
                "http://yikaoapp.com/share/?blue=dis=|action=list|from=|wxshare=true",
                  ]
    #获取区域url
    def parse(self, response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>Parse url= %s''' %response.url, level=log.INFO)
        sel = Selector(response)
        eara_url_list = sel.xpath("//div[@class='list_dis']/p/a")
        # for eara in eara_url_list[1:]:
        for eara in eara_url_list[1:2]: #测试一个地区
            eara_id = eara.xpath("@href").extract()[0].split("dis=")[1].split("|")[0]
            district = eara.xpath("text()").extract()[0]
            eara_url = "http://yikaoapp.com/share/ajax.php?page=1&from=&dis=" + eara_id + "&spc=&key="
            req =  Request(eara_url,callback=self.school_id_get)
            req.meta["district"] = district
            yield req

    def school_id_get(self,response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>school_id_get url= %s''' %response.url, level=log.INFO)
        date_all = json.loads(response.body)
        #获取学校的数量
        max_num = int(date_all[-1]["max"])
        if max_num == 0:
            pass
        else:
            # for date in date_all[:-1]: #最后一个是max
            for date in date_all[:2]: #只测试两个
                school_id = date["id"]
                school_name = date["name"]
                school_logo = date["logo"]
                school_url = "http://yikaoapp.com/share/?school_id=" + str(school_id)
                req = Request(school_url,callback=self.school_message_get)
                req.meta["district"] = response.meta["district"]
                req.meta["school_id"] = school_id  #学校id
                req.meta["school_name"] = school_name  #学校名字
                # req.meta["school_url"] = school_url  #学校url
                req.meta["school_logo"] = school_logo  #学校logo
                yield req

    def school_message_get(self,response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>school_message_get url= %s''' %response.url, level=log.INFO)
        sel = Selector(response)
        item = YikaoItem()
        school = sel.xpath("//div[@class='s_info']/div/text()").extract()
        item["school_type"] = school[0] #学校类型
        item["school_nature"] = school[1]  #学校性质
        item["school_recruitOrder"] = school[2] #录取批次
        item["school_id"] = response.meta["school_id"] #学校id
        item["school_name"] = response.meta["school_name"]  #学校名字
        item["school_logo"] = response.meta["school_logo"]  #学校logo
        item["school_district"] = response.meta["district"] #学校所属区域
        item["school_address"] = sel.xpath("//div[@class='s_top2']")[1].xpath(".//div[@class='s_item']/a/text()").extract()[1][3:]
        item["school_telephone"] = sel.xpath("//div[@class='s_top2']")[1].xpath(".//div[@class='s_item']/a/text()").extract()[2][3:]

        #招生简章
        school_enrollment_url = "http://yikaoapp.com/share/" + sel.xpath("//div[@class='s_jianzhang']/a/@href").extract()[0]
        r = requests.get(school_enrollment_url)
        item["school_enrollment"] = r.text

        #学校简介
        school_intro_url = "http://yikaoapp.com/share/" +sel.xpath("//div[@class='s_top2']")[1].xpath(".//div[@class='s_item']/a/@href").extract()[0]
        r = requests.get(school_intro_url)
        item["school_intro"] = r.text
        log.msg('''>>>>>>>>>>>>>>>>>>>>>item= %s''' %item, level=log.INFO)
        yield item












