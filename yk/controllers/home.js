var router = require('express').Router();
var models = require('../models');

router.get('/', function home(req, res, next) {
    res.render('index.html');
});

router.get('/school', function home(req, res, next) {
    res.render('school.html');
});

router.get('/ajaxSchoolList', function(req, res, next){
    var district_id = parseInt(req.query.district_id),
        majorcate_id = parseInt(req.query.majorcate_id), 
        page = parseInt(req.query.page);
    
    // if (district_id == '0' ){district_id='*'}
    // if (majorcate_id == '0'){majorcate_id='*'}
    
    console.log('district_id: ', district_id, 'majorcate_id: ', majorcate_id, 'page: ', page);
    
    models.school.queryByDistrictAndMajorcate(district_id, majorcate_id,  page,  function(err, rows, fields){
        console.log(err);
        if (err){
            res.json({retCode:1, msg:'server error!'});
        } 
        
        res.json({ retCode:0, msg:'ok', data:{ list:rows } });
    });
    
});

router.get('/ajaxSchoolDetail', function(req, res, next){
    var school_id = req.query.school_id;
    
    models.school.queryById(school_id, function(err, rows, fields){
        if (err){
            res.json({retCode:1, msg:'server error!'});
        } 
        
        res.json({ retCode:0, msg:'ok', data:rows[0] });
    });
    
});

router.get('/ajaxMajorList', function(req, res, next){
    var school_id = parseInt(req.query.school_id);
    models.major.queryBySchoolId(school_id, function(err, rows, fields){
        if (err){
            res.json({retCode:1, msg:'server error!'});
        } 
        
        res.json({ retCode:0, msg:'ok', data:{ list:rows } });
    });
    
});

router.get('/ajaxDistrictList', function(req, res, next){
    models.district.queryAll(function(err, rows, fields){
        if (err){
            res.json({retCode:1, msg:'server error!'});
        } 
        
        res.json({ retCode:0, msg:'ok', data:{ list:rows } });
    });
    
});

router.get('/ajaxMajorcateList', function(req, res, next){
    models.major.queryAll(function(err, rows, fields){
        if (err){
            res.json({retCode:1, msg:'server error!'});
        } 
        
        res.json({ retCode:0, msg:'ok', data:{ list:rows } });
    });
    
});

router.get('/ajaxMajorDetail', function(req, res, next){
    var major_id = parseInt(req.query.major_id);
    models.major.queryDetailById(major_id, function(err, rows, fields){
        if (err){
            res.json('404啦！');
        } 
        
        res.send(rows[0]['detail']);
    });
    
});

router.get('/ajaxSchoolIntro', function(req, res, next){
    var school_id = parseInt(req.query.school_id);
    console.log('school_id: ',school_id);
    models.school.queryIntroById(school_id, function(err, rows, fields){
        if (err){
            res.json('404啦！');
        } 
        
        res.send(rows[0]['intro']);
    });
    
});
router.get('/ajaxSchoolEnrollment', function(req, res, next){
    var school_id = parseInt(req.query.school_id);
    models.school.queryEnrollmentById(school_id, function(err, rows, fields){
        if (err){
            res.json('404啦！');
        } 
        
        res.send(rows[0]['enrollment']);
    });
    
});

module.exports = router;