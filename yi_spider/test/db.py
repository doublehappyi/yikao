#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
__author__ = 'yishuangxi'
import requests
from bs4 import BeautifulSoup
import codecs
import json
import MySQLdb


settings = {
    'host':'localhost',
    'user':'root',
    'passwd':'11111111',
    'db':'yk',
    'charset':'utf8',
    'table':{
        'district':'district',
        'school':'school',
        'major':'major',
        'majorCate':'majorCate'
    }
}

def getConnection():
    return MySQLdb.connect(host=settings['host'], user=settings['user'], passwd=settings['passwd'], db=settings['db'],charset=settings['charset'])

def getCursor(conn):
    return conn.cursor()