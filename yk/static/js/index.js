/**
 * Created by db on 15/12/7.
 */
(function ($) {
    var districtList = [], majorcateList = [];
    $(function () {
        init();
        addEvents();
    });
    
    var page = 1;
    function init() {
        //地区选择
        var $area = $('#area'), $major = $('#major');
        var old_district_id = 0, old_majorcate_id = 0;
        $.ajax({
            type: "GET",
            url: '/ajaxDistrictList',
            success: function (data) {
                var values = [];
                if (data.retCode === 0) {
                    districtList = data['data']['list'];
                    districtList.unshift({id: 0, name: '全部地区'});
                    for (var i = 0; i < districtList.length; i++) {
                        values.push(districtList[i]['name']);
                    }
                    initDistrictPicker(values);
                }
            }
        });

        //专业选择
        $.ajax({
            type: "GET",
            url: '/ajaxMajorcateList',
            success: function (data) {
                var values = [];
                if (data.retCode === 0) {
                    majorcateList = data['data']['list'];
                    majorcateList.unshift({id: 0, name: '全部专业'});
                    for (var i = 0; i < majorcateList.length; i++) {
                        values.push(majorcateList[i]['name']);
                    }
                    initMajorcatePicker(values);
                }
            }
        });

        //初次加载大学列表数据
        loadSchoolData({}, function(err, data){
            if (err){throw err;}
            var html = produceSchoolListHtml(data);
            $('#tpl_school_list_container').empty().append(html);
            page++;
        });

        function initDistrictPicker(values) {
            $('#area').picker({
                toolbarTemplate: '<header class="bar bar-nav">\
                    <button class="button button-link pull-right close-picker">确定</button>\
                    <h1 class="title">地区列表</h1>\
                    </header>',
                cols: [
                    {
                        textAlign: 'center',
                        values: values
                    }
                ],
                onClose: onCloseCallback
            });
        }

        function initMajorcatePicker(values) {
            $('#major').picker({
                toolbarTemplate: '<header class="bar bar-nav">\
                    <button class="button button-link pull-right close-picker">确定</button>\
                    <h1 class="title">专业列表</h1>\
                    </header>',
                cols: [
                    {
                        textAlign: 'center',
                        values: values
                    }
                ],
                onClose: onCloseCallback
            });
        }

        function onCloseCallback(){
            var district_id = get_district_id(districtList),
                majorcate_id = get_majorcate_id(majorcateList);
            //如果id没有发生变化，则不加载数据
            if(old_district_id === district_id && old_majorcate_id === majorcate_id) return;

            old_district_id = district_id, old_majorcate_id = majorcate_id;
            page = 1;
            var config = {page:page, district_id:district_id, majorcate_id:majorcate_id}
            loadSchoolData(config, function(err, data){
                if (err){throw err;}
                var html = produceSchoolListHtml(data);
                
                $('#tpl_school_list_container').empty().append(html);
                page++;
            });


        }
    }

    function addEvents() {
        //翻页加载数据
        (function () {
            var isLoading = false;
            $(".content").scroll(function () {
                // console.log('scrolling ...');
                if (isLoading) return;
                var $this = $(this),
                    viewH = $(this).height(),//可见高度
                    contentH = $(this).get(0).scrollHeight,//内容高度
                    scrollTop = $(this).scrollTop();//滚动高度

                if (scrollTop / (contentH - viewH) >= 0.95) { //到达底部100px时,加载新内容
                    console.log('ajaxing ...');
                    isLoading = true;
                    loadSchoolData({
                        district_id: get_district_id(districtList),
                        majorcate_id: get_majorcate_id(majorcateList),
                        page: page
                    }, function (err, data) {
                        isLoading = false;
                        if (err) {
                            throw err;
                        }
                        var html = produceSchoolListHtml(data);
                        $('#tpl_school_list_container').append(html);
                    });
                    page++;
                }
            });
        })();
    }

    function produceSchoolListHtml(data) {
        var tpl = $('#tpl_school_list').html();
        var html = Mustache.render(tpl, data);
        return html;
    }

    function loadSchoolData(config, callback) {
        var defaultConfig = {
            district_id: 0,
            majorcate_id: 0,
            page: 1
        };
        var config = $.extend({}, defaultConfig, config);
        $.ajax({
            url: '/ajaxSchoolList',
            type: 'GET',
            data: config,
            success: function (data) {
                if (data.retCode === 0 && data.data) {
                    callback(null, data.data);
                } else {
                    callback(new Error('loadSchoolData fail!'))
                }
            },
            error: function (err) {
                callback(err);
            }
        });
    }

    function get_district_id(districtList) {
        var areaName = $('#area').val(), district_id;
        for (var i = 0; i < districtList.length; i++) {
            if (districtList[i]['name'] === areaName) {
                district_id = districtList[i]['id'];
                break;
            }
        }

        return district_id;
    }

    function get_majorcate_id(majorcateList) {
        var majorName = $('#major').val(), majorcate_id;
        for (var i = 0; i < majorcateList.length; i++) {
            if (majorcateList[i]['name'] === majorName) {
                majorcate_id = majorcateList[i]['id'];
                break;
            }
        }

        return majorcate_id;
    }


})(Zepto);
