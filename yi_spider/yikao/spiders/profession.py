#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import Selector
from yikao.items import ProItem
from scrapy.http import Request
from scrapy import log
import json

class Mi2Spider(CrawlSpider):
    name = 'zhuanye'
    allowed_domains = ["yikaoapp.com"]
    start_urls = [
                "http://yikaoapp.com/share/?blue=dis=|action=list|from=|wxshare=true",
                  ]
    #获取区域url
    def parse(self, response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>Parse url= %s''' %response.url, level=log.INFO)
        sel = Selector(response)
        eara_url_list = sel.xpath("//div[@class='list_dis']/p/a")
        # for eara in eara_url_list[1:]:
        for eara in eara_url_list[2:3]: #测试一个地区
            eara_id = eara.xpath("@href").extract()[0].split("dis=")[1].split("|")[0]
            eara_url = "http://yikaoapp.com/share/ajax.php?page=1&from=&dis=" + eara_id + "&spc=&key="
            req =  Request(eara_url,callback=self.school_id_get)
            yield req

    def school_id_get(self,response):
        log.msg('''>>>>>>>>>>>>>>>>>>>>>school_id_get url= %s''' %response.url, level=log.INFO)
        date_all = json.loads(response.body)
        #获取学校的数量
        max_num = int(date_all[-1]["max"])
        if max_num == 0:
            pass
        else:
            # for date in date_all[:-1]: #最后一个是max
            for date in date_all[:2]: #只测试两个
                school_id = date["id"]
                school_url = "http://yikaoapp.com/share/?school_id=" + str(school_id)
                req = Request(school_url,callback=self.pro_url_get)
                req.meta["school_id"] = school_id  #学校id
                yield req

    def pro_url_get(self,response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>pro_url_get url= %s''' %response.url, level=log.INFO)
        sel = Selector(response)
        pro_message_list = sel.xpath("//div[@class='clearfix']")
        for pro in pro_message_list:
            pro_title = pro.xpath(".//div[@class='s_title']/span/text()").extract()[0]
            pro_name = pro.xpath(".//div[@class='s_item']/a/text()").extract()[0].strip()
            pro_url = "http://yikaoapp.com/share/" + pro.xpath(".//div[@class='s_item']/a/@href").extract()[0]
            req = Request(pro_url,callback=self.pro_message_get)
            req.meta["school_id"] = response.meta["school_id"]
            req.meta["title"] = pro_title
            req.meta["pro_name"] = pro_name
            yield req

    def pro_message_get(self,response):
        # log.msg('''>>>>>>>>>>>>>>>>>>>>>pro_message_get url= %s''' %response.url, level=log.INFO)
        item = ProItem()
        item["pro_type"] = self.pro_type_classify(response.meta["pro_name"]) #专业类别
        item["school_id"] = response.meta["school_id"] #学校id
        item["pro_id"] = response.url.split("specialty_id=")[1].split('|')[0] #专业id
        item["college_name"] = response.meta["title"] #学院名称
        item["pro_name"] =response.meta["pro_name"] #专业名称
        # item["pro_message"] = response.body #专业信息
        log.msg('''>>>>>>>>>>>>>>>>>>>>>pro_message_get message= %s''' %item, level=log.INFO)

        yield item


    def pro_type_classify(self,name):
        pro_type = ""
        if (u"音乐" or  u"声乐" or  u"美声") in name:
            pro_type = "音乐类"
        elif (u"播音" or u"主持") in name:
            pro_type = "播音类"
        elif (u"编导" or u"导演" or u"编辑") in name:
            pro_type = "编导类"
        elif u"舞蹈" in name:
            pro_type = "舞蹈类"
        elif (u"美术" or u"画"  or u"舞台") in name:
            pro_type = "美术类"
        elif (u"表演" or u"话剧") in name:
            pro_type = "表演类"
        elif (u"摄影" or u"光学" or u"照明") in name:
            pro_type = "摄影类"
        elif u"模特" in name:
            pro_type = "模特类"
        elif u"空乘" in name:
            pro_type = "空乘类"
        else:
            pro_type = "其它类"
        return pro_type




















