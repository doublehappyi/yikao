#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
__author__ = 'yishuangxi'
import requests
from bs4 import BeautifulSoup
import json
import db

def getSchoolItemList(districtList):
    schoolItemList = []

    for district in districtList:
        base_logo_url = 'http://i.yikaoapp.com/upload/school/'
        url = 'http://yikaoapp.com/share/ajax.php?dis='+str(district[0])
        flag = 1 #是否翻页请求
        while flag:
            r = requests.get(url)
            r.encoding = 'utf-8'
            dataList = json.loads(r.text)
            dataList = dataList[0:-1]
            for data in dataList:
                schoolItemList.append({
                    'id':data['id'],
                    'district_id':district[1],
                    'logo':base_logo_url + data['id'] + '/' + data['logo']
                })

            if len(dataList) < 40:
                #如果获取到的数据长度小于41条，则不再请求下一页
                flag = 0
    return schoolItemList

def getPageHtml(url):
    r = requests.get(url)
    r.encoding = 'utf-8'
    html = r.text
    return html

def getAndSave_SchooList_MajorList(schoolItemList):
    conn = db.getConnection()
    cursor = db.getCursor(conn)
    for schoolItem in schoolItemList:
        school_id = schoolItem['id'],
        district_id = schoolItem['district_id']

        url = 'http://yikaoapp.com/share/?school_id=' + str(school_id)
        r = requests.get(url)
        r.encoding = 'utf-8'
        html = r.text
        bs = BeautifulSoup(html, 'html.parser')
        insertSchoolItem(bs, conn, cursor, school_id, district_id)

def insertSchoolItem(bs, conn, cursor, school_id, district_id):
    base_url = 'http://yikaoapp.com/share/'
    school = {}
    school['id'] = school_id
    school['name'] = bs.title.get_text()
    print school['name'], school['id']
    school['logo'] = bs.select('#logoimg')[0].attrs['src']
    school['banner'] = bs.select('.s_img > img')[0].attrs['src']
    if len(bs.select('.s_tag a')) > 0:
        school['tag'] = bs.select('.s_tag a')[0].get_text()
    else:
        school['tag'] = ''

    infoItems1 = bs.select('.s_info > div')
    school['stype'] = infoItems1[0].get_text().split('：')[1]
    school['nature'] = infoItems1[1].get_text().split('：')[1]
    school['recruitOrder'] = infoItems1[2].get_text().split('：')[1]
    school['recruitTotalNum'] = infoItems1[3].get_text().split('：')[1]

    school['enrollment'] = getPageHtml(base_url + bs.select('.s_jianzhang > a')[0].attrs['href'])

    s_items = bs.select('.s_top2')[1].select('.s_item > a')
    school['intro'] = getPageHtml(base_url + s_items[0].attrs['href'])
    school['address'] = s_items[1].get_text().split('：')[1]
    school['telephone'] = s_items[2].get_text().split('：')[1]

    select_str = "select id from {0}".format(db.settings['table']['school']) + " where id=%s"
    if cursor.execute(select_str, (school['id'], )):
        return
    insert_str = "insert into {0}".format(db.settings['table']['school']) +  " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(insert_str, (school['id'],school['name'],school['logo'],\
                                school['banner'],school['tag'],school['stype'],\
                                school['nature'],school['recruitOrder'],school['enrollment'],\
                                school['intro'],school['address'],school['telephone'], district_id))
    conn.commit()


def insertMajorItem(bs, conn, cursor, school_id):
    base_url = 'http://yikaoapp.com/share/'
    majorItemBox = bs.select('.s_top2')[0]
    academyItems = majorItemBox.select('.clearfix')
    for academyItem in academyItems:
        title = academyItem.select('.s_title > span')[0].get_text()
        majorItems = academyItem.select('.s_item')
        for majorItem in majorItems:
            name = majorItem.select('a')[0].get_text().strip()
            recruit = majorItem.select('a > span')[0].get_text().split('：')[1]
            detail = getPageHtml(base_url + majorItem.select('a')[0].attrs['href'])
            academyName = title
            majorCate_id =  get_majorCate_id(name)
            school_id = school_id

            # select_str = "select id from {0}".format(db.settings['table']['school']) + " where name=%s"
            # if cursor.execute(select_str, (name, )):
            #     return
            insert_str = "insert into {0}".format(db.settings['table']['school']) +  " values (null, %s, %s, %s, %s, %s, %s)"
            cursor.execute(insert_str, (name, recruit, detail, academyName, majorCate_id, school_id))
            conn.commit()

def get_majorCate_id(name):
    #"音乐类":1, "播音类":2,"编导类":3,"舞蹈类":4 ,"美术类":5, "表演类":6, "摄影类":7, "模特类":8, "空乘类":9, "其它类":10
    pro_type = 0
    if (u"音乐" or  u"声乐" or  u"美声") in name:
        pro_type = 1
    elif (u"播音" or u"主持") in name:
        pro_type = 2
    elif (u"编导" or u"导演" or u"编辑") in name:
        pro_type = 3
    elif u"舞蹈" in name:
        pro_type = 4
    elif (u"美术" or u"画"  or u"舞台") in name:
        pro_type = 5
    elif (u"表演" or u"话剧") in name:
        pro_type = 6
    elif (u"摄影" or u"光学" or u"照明") in name:
        pro_type = 7
    elif u"模特" in name:
        pro_type = 8
    elif u"空乘" in name:
        pro_type = 9
    else:
        pro_type = 10

    return pro_type
