# coding:utf-8
__author__ = 'yishuangxi'
import sys

reload(sys)
sys.setdefaultencoding('utf8')

import requests
import codecs
import re


def get_html(url):
    r = requests.get(url)
    r.encoding = 'utf-8'
    html = r.text
    return html


def get_re_html(url):
    html = get_html(url)

    r_href = re.compile('href\s*=\s*"/')
    r_src = re.compile('src\s*=\s*"/')

    html = re.sub(r_href, 'href="http://yikaoapp.com/', html)
    html = re.sub(r_src, 'src="http://yikaoapp.com/', html)
    # f= codecs.open('test.html', 'w+')
    # f.write(html)
    # f.close()
    return html
