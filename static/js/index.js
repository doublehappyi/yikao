/**
 * Created by db on 15/12/7.
 */
(function ($) {
    var areaData = [], majorData = [];
    $(function () {
        init();
        addEvents();
    });

    function init() {
        //地区选择
        var $area = $('#area'), $major = $('#major');
        var oldAreaId = 0, oldMajorId = 0;
        $.ajax({
            type: "GET",
            url: './data/area.json',
            success: function (data) {
                var values = [];
                if (data.retCode === 0) {
                    areaData = data['data'];
                    areaData.unshift({id: 0, name: '全部地区'});
                    for (var i = 0; i < areaData.length; i++) {
                        values.push(areaData[i]['name']);
                    }
                    initAreaPicker(values);
                }
            }
        });

        //专业选择
        $.ajax({
            type: "GET",
            url: './data/major.json',
            success: function (data) {
                var values = [];
                if (data.retCode === 0) {
                    majorData = data['data'];
                    majorData.unshift({id: 0, name: '全部专业'});
                    for (var i = 0; i < majorData.length; i++) {
                        values.push(majorData[i]['name']);
                    }
                    initMajorPicker(values);
                }
            }
        });

        //初次加载大学列表数据
        loadUniversityData({}, function(err, data){
            if (err){throw err;}
            var html = produceUniversityListHtml(data);
            $('.i-list-box').empty().append(html);
        });

        function initAreaPicker(values) {
            $('#area').picker({
                toolbarTemplate: '<header class="bar bar-nav">\
                    <button class="button button-link pull-right close-picker">确定</button>\
                    <h1 class="title">地区列表</h1>\
                    </header>',
                cols: [
                    {
                        textAlign: 'center',
                        values: values
                    }
                ],
                onClose: onCloseCallback
            });
        }

        function initMajorPicker(values) {
            $('#major').picker({
                toolbarTemplate: '<header class="bar bar-nav">\
                    <button class="button button-link pull-right close-picker">确定</button>\
                    <h1 class="title">专业列表</h1>\
                    </header>',
                cols: [
                    {
                        textAlign: 'center',
                        values: values
                    }
                ],
                onClose: onCloseCallback
            });
        }

        function onCloseCallback(){
            var areaId = getAreaId(areaData),
                majorId = getMajorId(majorData);
            //如果id没有发生变化，则不加载数据
            if(oldAreaId === areaId && oldMajorId === majorId) return;

            oldAreaId = areaId, oldMajorId = majorId;

            var config = {pageNum:1, areaId:areaId, majorId:majorId}
            loadUniversityData(config, function(err, data){
                if (err){throw err;}
                var html = produceUniversityListHtml(data);
                $('.i-list-box').empty().append(html);
            });


        }
    }

    function addEvents() {
        //翻页加载数据
        (function () {
            var isLoading = false;
            var pageNum = 1;
            $(".content").scroll(function () {
                console.log('scrolling ...');
                if (isLoading) return;
                var $this = $(this),
                    viewH = $(this).height(),//可见高度
                    contentH = $(this).get(0).scrollHeight,//内容高度
                    scrollTop = $(this).scrollTop();//滚动高度

                if (scrollTop / (contentH - viewH) >= 0.95) { //到达底部100px时,加载新内容
                    console.log('ajaxing ...');
                    isLoading = true;
                    loadUniversityData({
                        areaId: getAreaId(areaData),
                        majorId: getMajorId(majorData),
                        pageNum: 1
                    }, function (err, data) {
                        isLoading = false;
                        if (err) {
                            throw err;
                        }
                        var html = produceUniversityListHtml(data);
                        $('.i-list-box').append(html);
                    });
                }
            });
        })();
    }

    function produceUniversityListHtml(data) {
        var tpl = $('#tpl_university_item').html();
        var html = Mustache.render(tpl, data);
        return html;
    }

    function loadUniversityData(config, callback) {
        var defaultConfig = {
            areaId: 0,
            majorId: 0,
            pageNum: 1
        };
        var config = $.extend({}, defaultConfig, config);
        $.ajax({
            url: 'data/universityList.json',
            type: 'GET',
            data: config,
            success: function (data) {
                if (data.retCode === 0 && data.data) {
                    callback(null, data.data);
                } else {
                    callback(new Error('loadUniversityData fail!'))
                }
            },
            error: function (err) {
                callback(err);
            }
        });
    }

    function getAreaId(areaData) {
        var areaName = $('#area').val(), areaId;
        for (var i = 0; i < areaData.length; i++) {
            if (areaData[i]['name'] === areaName) {
                areaId = areaData[i]['id'];
                break;
            }
        }

        return areaId;
    }

    function getMajorId(majorData) {
        var majorName = $('#major').val(), majorId;
        for (var i = 0; i < majorData.length; i++) {
            if (majorData[i]['name'] === majorName) {
                majorId = majorData[i]['id'];
                break;
            }
        }

        return majorId;
    }


})(Zepto);
