#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import requests
from bs4 import BeautifulSoup
import db

def getDistrictList():
    districtList = []
    url = 'http://yikaoapp.com/share/?blue=spc=|action=list|from=|wxshare=true'
    r = requests.get(url)
    r.encoding = 'utf-8'
    html = r.text
    bs = BeautifulSoup(html, 'html.parser')
    items = bs.select('.list_dis > p > a')[1:]
    for item in items:
        row = (item.attrs['href'].split('|')[0].split('=')[-1], item.get_text())
        districtList.append(row)

    return  districtList

def save(rows):
    conn = db.getConnection()
    cursor = db.getCursor(conn)
    
    for row in rows:
        dis_id = row[0]
        # print dis_id
        select_str = "select id from {0}".format(db.settings['table']['district']) + " where num=%s"
        # print select_str
        if cursor.execute(select_str, (dis_id, ) ):
            continue
        insert_str = "insert into {0}".format(db.settings['table']['district']) +  " values (null, %s, %s)"
        # print insert_str
        cursor.execute(insert_str, row)
        conn.commit()

    conn.close()
    cursor.close()
