/**
 * Created by yishuangxi on 2015/12/8.
 */
(function($){
    $(function(){
        init();
    });

    function init(){
        var id = getSearchValue('id');
        $.ajax({
            url:'data/universityInfo.json',
            type:'GET',
            data:{id:id},
            success:function(data){
                if(data && data.retCode === 0){
                    renderUniversityInfo(data.data);
                }
            }
        });

        $.ajax({
            url:'data/majorList.json',
            type:'GET',
            data:{id:id},
            success:function(data){
                if(data && data.retCode === 0){
                    var recruitTotalNum = 0, list = data.data.list;

                    for(var i = 0; i < list.length; i++){
                        recruitTotalNum += (list[i]['recruitNum']||0) + (list[i]['femaleNum']||0) + (list[i]['maleNum']||0);
                    }
                    renderRecruitTotalNum(recruitTotalNum);
                    var resultData = processMajorListData(data.data);
                    renderMajorList(resultData);

                }
            }
        });
    }

    var getSearchValue = (function(){
        var arr = location.search.replace(/^\?/, '').split('&');
        var obj = {}, pair;
        for(var i = 0; i < arr.length; i++){
            pair = arr[i].split('=');
            obj[pair[0]] = pair[1];
        }
        return function(key){
            return obj[key] || '';
        };
    })();

    function  renderUniversityInfo(data){
        var tpl1 = $('#tpl_universityInfo1').html();
        var tpl2 = $('#tpl_universityInfo2').html();
        var tpl3 = $('#tpl_universityName').html();

        var html1 = Mustache.render(tpl1, data);
        var html2 = Mustache.render(tpl2, data);
        var html3 = Mustache.render(tpl3, data);

        $('#tpl_universityInfo1_box').append(html1);
        $('#tpl_universityInfo2_box').append(html2);
        $('#tpl_universityName_box1').append(html3);
        $('#tpl_universityName_box2').append(html3);
    }

    function  renderMajorList(data){
        var tpl = $('#tpl_majorList').html();
        var html = Mustache.render(tpl, data);
        $('#tpl_majorList_box').append(html);
    }

    function renderRecruitTotalNum(num){
        var tpl = $('#tpl_recruitTotalNum').html();
        var html = Mustache.render(tpl, {recruitTotalNum:num});
        $('#tpl_recruitTotalNum_box').append(html);
    }

    var data = [{
        recruitTotalNum:0,
        list:[{},{}]
    }]

    function processMajorListData(data){
        var academyData = {};
        var academyList = [];
        academyData['academyList'] = academyList;
        var list = data['list'];
        for(var i = 0; i < list.length; i++){
            var academyIndex = academyNameInList(academyList, list[i]['academyName']);
            if(academyIndex < 0){
                academyList.push({academyName:list[i]['academyName'], majorList:[list[i]]});
            }else{
                academyList[academyIndex]['majorList'].push(list[i]);
            }
        }
        //console.log(academyList);
        //计算专业总人数,同时，把maleNum, femalNum, recruitNum为0的数据都转成null以供mustache模版做判断使用
        var recruitTotalNum = 0;
        for(var i = 0 ; i < list.length; i++){
            recruitTotalNum += list[i]['recruitNum'] + list[i]['femaleNum']+list[i]['maleNum'];
            //list[i]['recruitNum'] = list[i]['recruitNum'] === 0 ? null : list[i]['recruitNum'];
            //list[i]['femaleNum'] = list[i]['femaleNum'] === 0 ? null : list[i]['femaleNum'];
            //list[i]['maleNum'] = list[i]['maleNum'] === 0 ? null : list[i]['maleNum'];
        }



        academyData['majorNum'] = list.length;

        return academyData;
    }

    function academyNameInList(list, name){
        for(var i = 0; i < list.length; i++){
            if(list[i] && list[i]["academyName"] && list[i]["academyName"] === name){
                return i;
            }
        }
        return -1;
    }
})(Zepto);