#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from bs4 import BeautifulSoup
import db
import utils

if __name__ == '__main__':
    url = 'http://yikaoapp.com/share/?blue=spc=|action=list|from=|wxshare=true'
    html = utils.get_html(url)

    bs = BeautifulSoup(html, 'html.parser')
    dis_items = bs.select('.list_dis > p > a')
    conn = db.getConnection()
    cursor = db.getCursor(conn)
    dis_items = dis_items[1:]

    for item in dis_items:
        num = item.attrs['href'].split('|')[0].split('=')[-1]
        name = item.get_text()
        print num, name
        select_str = "select id from {0}".format(db.settings['table']['district']) + " where num=%s"
        if cursor.execute(select_str, (num, ) ):
            continue
        insert_str = "insert into {0}".format(db.settings['table']['district']) +  " values (null, %s, %s)"
        # print insert_str
        cursor.execute(insert_str, (num, name))
        conn.commit()

    conn.close()
    cursor.close()


