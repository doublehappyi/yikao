var mysql = require('mysql');
var conf = { 
    host:'127.0.0.1',
    port:3360,
    user:'yisx',
    password:'',
    database:'yk'
}

var table = {
    school:'school',
    major:'major',
    majorcate:'majorcate',
    district:'district',
    school_majorcate_district:'school_majorcate_district'
}

var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : conf.host,
  user            : conf.user,
  password        : conf.password,
  database        : conf.database
});

module.exports = {
    table:table,
    pool:pool
}