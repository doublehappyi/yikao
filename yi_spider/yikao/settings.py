# -*- coding: utf-8 -*-

# Scrapy settings for yikao project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'yikao'

SPIDER_MODULES = ['yikao.spiders']
NEWSPIDER_MODULE = 'yikao.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'yikao (+http://www.yourdomain.com)'



LOG_ENABLED = True
LOG_ENCODING = 'utf-8'
LOG_FILE = './yishu.log'
LOG_LEVEL = 'INFO'
LOG_STDOUT = True