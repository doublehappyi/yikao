# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item,Field


class YikaoItem(Item):
    # define the fields for your item here like:
    school_id = Field()
    school_name = Field() 
    school_logo = Field() 
    school_district = Field()
    school_type = Field() 
    school_nature = Field() 
    school_recruitOrder = Field()
    school_address = Field() 
    school_telephone = Field() 
    school_enrollment = Field() 
    school_intro = Field() 
    pass

class ProItem(Item):
    # define the fields for your item here like:
    pro_type = Field()
    school_id = Field()
    pro_id = Field()
    college_name = Field()
    pro_name = Field()
    pro_message = Field()
    pass

