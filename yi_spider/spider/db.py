#coding:utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
__author__ = 'yishuangxi'
import codecs
import json
import MySQLdb


# settings = {
#     'host':'localhost',
#     'user':'root',
#     'passwd':'11111111',
#     'db':'yk',
#     'charset':'utf8',
#     'table':{
#         'district':'district',
#         'school':'school',
#         'major':'major',
#         'majorCate':'majorCate'
#     }
# }

settings = {
    'host':'127.0.0.1',
    'user':'yisx',
    'passwd':'',
    'db':'yk',
    'charset':'utf8',
    'table':{
        'district':'district',
        'school':'school',
        'major':'major',
        'majorCate':'majorCate'
    }
}


def getConnection():
    return MySQLdb.connect(host=settings['host'], user=settings['user'], db=settings['db'],charset=settings['charset'])
    
# def getConnection():
#     return MySQLdb.connect(host=settings['host'], user=settings['user'], passwd=settings['passwd'], db=settings['db'],charset=settings['charset'])

def getCursor(conn):
    return conn.cursor()