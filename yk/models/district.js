var db = require('../conf').db;
var table = db.table,
    pool = db.pool;
    
var sql = {
    queryAll:'select id, name from ' + db.table.district + ' order by id'
}

module.exports = {
    queryAll:function(callback){
        pool.getConnection(function(err, conn){
            if(err){callback(err);return;}
            conn.query(sql.queryAll, function(err, rows, fields){
                if(err){ callback(err); return;}
                callback(null, rows, fields);
                conn.release();
            });
        });
    }
}